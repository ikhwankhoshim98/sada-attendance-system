@extends('layouts.app1')
<html>
    <head>

    </head>
        <body>
            <div class="container">
                <br />
                <h3 align="center">SADA ATTENDANCE REPORT</h3>
                <br />
                    <br />
                        <form action="/attendanceFilter" method = "GET">
                            @csrf
                            <div class="row input-attendanceFilter">
                            <div class="col-md-4">

                            <label for="start">Select Department</label>
                            <select name="searchCustom" id="searchCustom" class="form-control input-lg dynamic" data-dependent="jabatan">
                            <option value= "">All Departments</option>
                            @foreach($dropdownData as $jabatan)
                            <option value= "{{$jabatan-> jabatan }}"  {{ request()->get("searchCustom") == $jabatan-> jabatan  ? "selected" : "" }}>{{ $jabatan-> jabatan }}</option>
                            @endforeach
                            </select>
                            </div>

                            <div class="col-md-4">

                            <label for="start">Select Staff</label>
                            <select name="searchCustom2" id="searchCustom2" class="form-control input-lg dynamic" data-dependent="jabatan">
                            <option value= "">All Staff</option>
                            @foreach($dropdownData2 as $jabatan)
                            <option value= "{{$jabatan-> pin }}" {{ request()->get("searchCustom2") == $jabatan-> pin  ? "selected" : "" }}>{{ $jabatan-> pin }} {{ $jabatan-> nama }}</option>
                            @endforeach
                            </select>
                            </div>
                            </div>


                            <div class="row input-attendanceFilter">
                            <div class="col-md-4">

                            <label for="from_date">Start date:</label>
                            <input type="date" name="from_date" id="from_date" class="form-control" placeholder="From Date" value= "<?php if (isset($_GET['from_date'])) echo $_GET['from_date']; ?>"/>

                            <label for="to_date">End date:</label>
                            <input type="date" name="to_date" id="to_date" class="form-control" placeholder="To Date" value= "<?php if (isset($_GET['to_date'])) echo $_GET['to_date']; ?>"  />
                            <button type="submit"  class="btn btn-primary">submit</button>

                            </div>
                            </div>

                        </form>
                    <br />



            <div class="table-responsive">
                    <table  id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <td>Pin</td>
                                    <td>Nama</td>
                                    <td>Jabatan</td>
                                    <td>Tarikh</td>
                                    <td>Masuk</td>
                                    <td>Keluar</td>
                                </tr>
                            </thead>

                        @foreach($customJabatan as $report)
                            <tr>
                                    <td>{{$report ->pin}}</td>
                                    <td>{{$report -> nama}}</td>
                                    <td>{{$report -> jabatan}}</td>
                                    <td>{{$report -> tarikh}}</td>
                                    <td>{{$report -> keluar}}</td>
                                    <td>{{$report -> masuk}}</td>
                            </tr>
                            @endforeach
                </table>


            </style>

            <div>

                {{$customJabatan -> appends (Request::except('pagjfhffge')) ->links()}}


            <style>
                .w-5
                {
                    display:none
                }
            </style>
            </div>


            </div>
            </div>



        </body>
</html>
