<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\report;
use Illuminate\Support\Facades\DB;

class attendanceFilterController extends Controller
{
    public function viewAll(Request $request)
    {
      //for dropdown list(8 department option)
      
      
      $dropdownData = DB::table('empdetails')
      ->select('jabatan')
      ->groupBy('jabatan')
      ->get();
       
      $dropdownData2 = DB::table('empdetails')
      ->select('pin', 'nama')
      ->groupBy('pin', 'nama')
      ->get();

         
      //view all database without filter yet
      $customJabatan = DB::table('reports')
      ->select()
      ->whereDate('tarikh', '>=', \Carbon\Carbon::today()-> subMonths(4))
      ->paginate(10);
      
      return view('attendanceFilter', compact('dropdownData', 'dropdownData2', 'customJabatan'));
    }

    public function requestCustomAttendance(Request $requestCustom)
    {

      if ($requestCustom -> searchCustom != "" && $requestCustom -> searchCustom2 != "")
      {
              $customJabatan = DB::table('reports')
              ->select()
              ->where('jabatan', array($requestCustom->searchCustom))
              ->where('pin', array($requestCustom->searchCustom2))
              ->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date))
              ->paginate(10);
              
              
      }

      else if ($requestCustom -> searchCustom == "" && $requestCustom -> searchCustom2 == "")
          {
              $customJabatan = DB::table('reports')
              ->select()
              ->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date))
              ->paginate(10);
              
            

          }

      else if ($requestCustom -> searchCustom == "" && $requestCustom -> searchCustom2 !="")
          {
              $customJabatan = DB::table('reports')
              ->select()
              ->where('pin', array($requestCustom->searchCustom2))
              ->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date))
              ->paginate(10);
              
              
          }

       else if ($requestCustom -> searchCustom != "" && $requestCustom -> searchCustom2 == "")
          {
              $customJabatan = DB::table('reports')
              ->select()
              ->where('jabatan', array($requestCustom->searchCustom))
              ->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date))
              ->paginate(10);
             

          }

        else
        {
              $customJabatan = DB::table('reports')
              ->select()
              ->where('jabatan', 'PENTABIRAN')
              ->paginate(10);
              
        }
            $dropdownData = DB::table('reports')
          ->select('jabatan')
          ->groupBy('jabatan')
          ->get();
          
          $dropdownData2 = DB::table('reports')
          ->select('pin', 'nama')
          ->groupBy('pin', 'nama')
          ->get();

        
          return view('attendanceFilter', compact('customJabatan', 'dropdownData', 'dropdownData2'));
          
    }
        
   
}
    



