<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\report;
// use App\Models\empdetail;
use Illuminate\Support\Facades\DB;


class absenceFilterController extends Controller
{
    public function viewAllAbsence()
    {
      //for dropdown list(8 department option)
      $dropdownData = DB::table('empdetails')
      ->select('jabatan')
      ->groupBy('jabatan')
      ->get();
       
      $dropdownData2 = DB::table('empdetails')
      ->select('pin', 'nama')
      ->groupBy('pin', 'nama')
      ->get();

         
      //view all database without filter yet
      $customAbsence = DB::table('empdetails')
      ->select('pin', 'nama', 'jabatan')
      ->where('status', '=', 'AVL')
      ->whereNotIn('nama', DB::table('reports')->select('nama')->whereDate('tarikh', '=', '2021-01-03'))
      ->paginate(10);
      
      return view('absenceFilter', compact('dropdownData', 'dropdownData2', 'customAbsence'));
    }

    public function requestCustomAbsence(Request $requestCustom) 
    {
     

      if ($requestCustom -> searchCustom != "" && $requestCustom -> searchCustom2 != "")
      {
              $customAbsence = DB::table('empdetails')
              ->select('pin', 'nama', 'jabatan')
              ->where('status', '=', 'AVL')
              ->where('jabatan', array($requestCustom->searchCustom))
              ->where('pin', array($requestCustom->searchCustom2)) 
              ->whereNotIn('nama', DB::table('reports')->select('nama')->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date)))
              ->paginate(10);
              
  
              
              
              
      }

      else if ($requestCustom -> searchCustom == "" && $requestCustom -> searchCustom2 == "")
          {
              $customAbsence = DB::table('empdetails')
              ->select('pin', 'nama', 'jabatan')
              ->where('status', '=', 'AVL')
              ->whereNotIn('nama', DB::table('reports')->select('nama')->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date)))
              ->paginate(10);
            

          }

      else if ($requestCustom -> searchCustom == "" && $requestCustom -> searchCustom2 !="")
          {
              $customAbsence = DB::table('empdetails')
              ->select('pin', 'nama', 'jabatan')
              ->where('status', '=', 'AVL')
              ->where('pin', array($requestCustom->searchCustom2)) 
              ->whereNotIn('nama', DB::table('reports')->select('nama')->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date)))
              ->paginate(10);
              
              
          }

       else if ($requestCustom -> searchCustom != "" && $requestCustom -> searchCustom2 == "")
          {
              $customAbsence = DB::table('empdetails')
              ->select('pin', 'nama', 'jabatan')
              ->where('status', '=', 'AVL')
              ->where('jabatan', array($requestCustom->searchCustom)) 
              ->whereNotIn('nama', DB::table('reports')->select('nama')->whereBetween('tarikh', array($requestCustom->from_date, $requestCustom->to_date)))
              ->paginate(10);
              

          }

        else
        {

              
        }
            $dropdownData = DB::table('empdetails')
            ->select('jabatan')
            ->groupBy('jabatan')
            ->get();
          
            $dropdownData2 = DB::table('empdetails')
            ->select('pin', 'nama')
            ->groupBy('pin', 'nama')
            ->get();

        
            return view('absenceFilter', compact('customAbsence', 'dropdownData', 'dropdownData2'));
          
    }
        
}
